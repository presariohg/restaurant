from django.contrib.auth.forms import UserCreationForm
from django.core.handlers.wsgi import WSGIRequest
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse_lazy, reverse
from django.views import generic
from django.contrib.auth import views
from django.contrib.auth.models import User

class LoginView(views.LoginView):
    template_name = 'internal/auth/login.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['site_header'] = "Verwaltung"

        return context


    def get(self, request:WSGIRequest, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse("auth:logged_in"))
        else:
            return super().get(request, *args, **kwargs)


class PasswordChangeView(views.PasswordChangeView):
    template_name = "internal/auth/password_change.html"
    success_url = reverse_lazy("auth:password_change_done")

    def get(self, request:WSGIRequest, *args, **kwargs):
        if request.user.is_authenticated:
            return super().get(request, *args, **kwargs)

        return HttpResponseRedirect(reverse("auth:login"), status=401)


class ProfileView(generic.TemplateView):
    template_name = 'internal/auth/logged_in.html'


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_superuser:
            context['accounts'] = User.objects.all()

        return context


    def get(self, request:WSGIRequest, *args, **kwargs):
        if request.user.is_authenticated:
            if request.user.is_superuser: # only allow user deletion for su
                if 'delete' in request.GET.keys():
                    deleting_id = int(request.GET['delete'])
                    try:
                        deleting_user = User.objects.get(id=deleting_id)

                        if not deleting_user.is_superuser: # su cannot be deleted
                            deleting_user.delete()
                    except:
                        pass

            return super().get(request, *args, **kwargs)

        return HttpResponseRedirect(reverse("auth:login"), status=401)


class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("auth:login")
    template_name = "internal/auth/signup.html"


    def get(self, request:WSGIRequest, *args, **kwargs):
        if request.user.is_superuser:
            return super().get(request, *args, **kwargs)

        return HttpResponseRedirect(reverse("auth:login"))


class EmailChangeView(generic.TemplateView):
    template_name = "internal/auth/change_email.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_superuser:
            context['email_admin_current'] = User.objects.all().get(username='admin').email

        return context


    def get(self, request:WSGIRequest, *args, **kwargs):
        if request.user.is_authenticated:
            if request.user.is_superuser:
                return super().get(request, *args, **kwargs)
            else:
                return HttpResponseRedirect(reverse("auth:logged_in"), status=403)
        else:
            return HttpResponseRedirect(reverse("home:home"), status=401)

    @staticmethod
    def post(request:WSGIRequest):
        if request.user.is_authenticated:
            if request.user.is_superuser:
                try:
                    new_admin_email = request.POST['email_admin']
                    admin = User.objects.get(username='admin')
                    admin.email = new_admin_email
                    admin.save()
                    return HttpResponseRedirect(reverse("auth:logged_in"), status=200)
                except:
                    return HttpResponse(status=400)
            else:
                return HttpResponseRedirect(reverse("auth:logged_in"), status=403)
        else:
            return HttpResponseRedirect(reverse("home:home"), status=401)