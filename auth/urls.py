from django.urls import path

from auth.views import LoginView, ProfileView, SignUpView, PasswordChangeView, EmailChangeView
from django.contrib.auth.views import LogoutView, PasswordChangeDoneView

from menu.views import MenuEditView

app_name = 'auth'
urlpatterns = [
    path('',
         LoginView.as_view(),
         name='login'),

    path('logout/',
         LogoutView.as_view(template_name="internal/auth/logged_out.html"),
         name='logout'),

    path('password_change/',
         PasswordChangeView.as_view(),
         name="password_change"),

    path('password_change/done/',
         PasswordChangeDoneView.as_view(template_name="internal/auth/password_change_done.html"),
         name="password_change_done"),


    path('signup/', SignUpView.as_view(), name='sign_up'),
    path('management/', ProfileView.as_view(), name='logged_in'),


    path('edit_menu/', MenuEditView.as_view(), name='edit_menu'),
    path('change_email', EmailChangeView.as_view(), name='change_email')
]