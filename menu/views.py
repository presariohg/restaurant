# from django.shortcuts import render
from django.core.handlers.wsgi import WSGIRequest
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic

from reservation.views import get_time_opening


class MenuView(generic.TemplateView):
    template_name =  'external/menu.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['food'] = {
            "name"          : "Kale Chips Art Party",
            "img"           : 'img/breakfast_item.jpg',
            "price"         : 7.5,
            "description"   : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea"
        }


        context['timeslots'] = get_time_opening().strftime("%H:%M")

        return context

class MenuEditView(generic.TemplateView):
    template_name = 'internal/menu.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context

    def get(self, request:WSGIRequest, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseRedirect(reverse("auth:login"))

        return super().get(request, *args, **kwargs)