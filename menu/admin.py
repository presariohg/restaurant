from django.contrib import admin

from menu.models import Dish, Topping, DishTopping, Tag, ToppingTag, DishTag

# Register your models here.
admin.site.register([Dish, Topping, DishTopping, Tag, ToppingTag, DishTag])