from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models


class Item(models.Model):
    name = models.CharField(max_length=200)
    sub_name = models.CharField(max_length=200, null=True, blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs): # default to None for nullable fields
        if not self.sub_name:
            self.sub_name = None

        super().save(*args, **kwargs)


class FoodItem(Item):
    price = models.FloatField(validators=[MinValueValidator(0)])
    is_vegan = models.BooleanField(default=False)
    spicy_level = models.PositiveSmallIntegerField(validators=[MaxValueValidator(5)], default=0)

    class Meta:
        abstract = True


class Tag(models.Model):
    text = models.CharField(max_length=50)
    sub_text = models.CharField(max_length=50, default="")

    def __str__(self):
        return self.text


class Topping(FoodItem): # toppings ain't got no photos
    pass


class Dish(FoodItem): # dishes do2
    img = models.ImageField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    sub_description = models.TextField(null=True, blank=True)

    def save(self, *args, **kwargs): # default to None for nullable fields
        if not self.img:
            self.img = None

        if not self.description:
            self.description = None

        if not self.sub_description:
            self.sub_description = None

        super().save(*args, **kwargs)


class DishTopping(models.Model):
    dish = models.ForeignKey(to=Dish, on_delete=models.CASCADE)
    topping = models.ForeignKey(to=Topping, on_delete=models.CASCADE)
    price = models.FloatField(validators=[MinValueValidator(0)], null=True)

    class Meta:
        unique_together = ("dish", "topping")

    def __str__(self):
        return f"{self.dish} - {self.topping}"


class DishTag(models.Model):
    dish = models.ForeignKey(to=Dish, on_delete=models.CASCADE)
    tag = models.ForeignKey(to=Tag, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.dish} - {self.tag}"

class ToppingTag(models.Model):
    topping = models.ForeignKey(to=Topping, on_delete=models.CASCADE)
    tag = models.ForeignKey(to=Tag, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.topping} - {self.tag}"
