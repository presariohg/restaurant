# from django.shortcuts import render
from django.views import generic

from reservation.views import get_time_opening

class HomeView(generic.TemplateView):
    template_name = 'external/home.html'


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['timeslots'] = get_time_opening().strftime("%H:%M")

        return context


class ContactView(generic.TemplateView):
    template_name = 'external/contacts.html'


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['timeslots'] = get_time_opening().strftime("%H:%M")

        return context