from django.urls import path

from reservation.views import ReservationView, ajax_reservation, ajax_get_available_tables

app_name = 'reservation'
urlpatterns = [
    path('', ReservationView.as_view(), name='home'),
    path('reserve', ajax_reservation, name="ajax_reservation"),
    path('get-table', ajax_get_available_tables, name='ajax_get_available_tables')
]