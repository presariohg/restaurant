from django.contrib import admin

from reservation.models import Table, Reservation

# Register your models here.
admin.site.register([Table, Reservation])