from django.db import models

import pandas as pd


class Table(models.Model):
    table_name = models.CharField(max_length=5, unique=True)
    seat_count = models.PositiveIntegerField(default=2)

    def __str__(self):
        return f"T{self.table_name} ({self.seat_count}x)"

    def is_free_at(self, timestamp:pd.Timestamp)->bool:
        """
        Check if this table is free at this timestamp
        :param timestamp:
        :return:
        """

        reservation_tables = ReservationTable.objects.filter(table=self, reservation__time_reserved__day=timestamp.day)
        for reservation_table in reservation_tables:
            reservation = reservation_table.reservation

            time_start = reservation.time_start or reservation.time_reserved # true start time or expected (if not come yet)
            time_end = reservation.time_end or (reservation.time_reserved + pd.Timedelta(hours=2))

            is_inbetween = time_start <= timestamp < time_end # should use <= at the 2nd position?

            if is_inbetween:
                return False

        return True


    @staticmethod
    def get_free_at(timestamp:pd.Timestamp)->list:
        """
        Get all free table at this timestamp
        :param timestamp:
        :return:
        """

        tables = Table.objects.all()
        unassigned = []

        for table in tables:
            if table.is_free_at(timestamp):
                unassigned.append(table)

        return unassigned


class Reservation(models.Model):
    customer_name = models.CharField(max_length=20)
    customer_phone = models.CharField(max_length=15, default="Leer", null=True)


    time_reserved = models.DateTimeField() # time_start == null => reserved
    time_start = models.DateTimeField(null=True) # time end == null && time_start != null => occupied
    time_end = models.DateTimeField(null=True) # time end < current => free

    seat_count = models.PositiveSmallIntegerField()

    def __str__(self):
        assigned_tables = self.tables

        date_str = f"{self.time_reserved.strftime('%Y-%m-%d %H:%M')}"
        date_today = pd.Timestamp.now().strftime('%Y-%m-%d')
        if date_today in date_str:
            date_str = date_str.replace(date_today, 'Heute')

        if self.time_end:
            date_str += self.time_end.strftime(" - %H:%M")

        date_str += f" - {self.customer_name}"

        if assigned_tables:
            tables = ""
            for table in assigned_tables:
                tables += f"T{table.table_name}, "

            date_str += f" ({tables[:-2]})" # remove last 2 char (, and space)
        else:
            date_str += f" ({self.seat_count}x)"


        return date_str


    def get_reserved_time_str(self)->str:
        date_reserved = self.time_reserved.date()
        date_today = pd.Timestamp.now().date()

        if date_today == date_reserved:
            return f'heute um {self.time_reserved.strftime("%H:%M")}'
        else:
            return self.time_reserved.strftime("%d.%m um %H:%M")


    @property
    def tables(self)->list[Table]:
        try:
            return ReservationTable.get_tables(reservation=self)
        except ReservationTable.DoesNotExist:
            return []


    @staticmethod
    def get_unassigned(): # reserved but not assigned to a table yet
        today = pd.Timestamp.now().floor('D')
        reservations_today = Reservation.objects.filter(time_reserved__gte=today)
        unassigned = []

        for reservation in reservations_today:
            started = reservation.time_start
            assigned = reservation.tables

            if (not assigned) and (not started):
                unassigned.append(reservation)

        return unassigned


    @staticmethod
    def get_waiting(): # assigned to a table, but hasn't come yet
        today = pd.Timestamp.now().floor('D')
        reservations_today = Reservation.objects.filter(time_reserved__gte=today)

        waiting = []
        for reservation in reservations_today:
            started = reservation.time_start
            assigned = reservation.tables

            if assigned and (not started):
                waiting.append(reservation)

        return waiting


    @staticmethod
    def get_on_going(): # came, but hasn't finished yet
        today = pd.Timestamp.now().floor('D')
        reservations_today = Reservation.objects.filter(time_reserved__gte=today)

        return reservations_today.filter(time_start__isnull=False, time_end__isnull=True)


    @staticmethod
    def get_assigned(): # assigned to a table, don't care current status
        today = pd.Timestamp.now().floor('D')
        reservations_today = Reservation.objects.filter(time_reserved__gte=today)

        assigned = []
        for reservation in reservations_today:
            if reservation.tables:
                assigned.append(reservation)

        return assigned


class ReservationTable(models.Model):
    reservation = models.ForeignKey(to=Reservation, on_delete=models.CASCADE)
    table = models.ForeignKey(to=Table, on_delete=models.CASCADE)


    def __str__(self):
        return f"{self.reservation}, table {self.table.table_name}"


    @staticmethod
    def get_tables(reservation:Reservation=None, reservation_id:int=None)->list:
        """
        Get a list of tables assigned to a reservation. The list is empty if the given reservation hasn't been assigned
        to any table
        :param reservation:
        :param reservation_id:
        :return:
        """
        if not reservation_id and not reservation:
            raise TypeError("Must input at least a Reservation object or an ID")

        if reservation_id: # can raise a type error if the id is wrong
            reservation = Reservation.objects.get(id=reservation_id)

        try:
            values = ReservationTable.objects.filter(reservation=reservation).values_list('table')
            return [Table.objects.get(id=field[0]) for field in values]
        except ReservationTable.DoesNotExist:
            return []