import traceback

import pandas as pd
from django.core.handlers.wsgi import WSGIRequest
from django.http import HttpResponseRedirect, HttpResponse, QueryDict, JsonResponse
from django.urls import reverse
from django.views import generic

from reservation.models import Table, Reservation, ReservationTable


class _Cell:
    def __init__(self, content, color):
        self.content = content
        self.color = color

    def __str__(self):
        return str(self.content)


class ReservationView(generic.TemplateView):
    template_name = 'internal/reservation.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['today'] = pd.Timestamp.now().strftime("%Y-%m-%d")
        context['reservations'] = {
            'unassigned' : Reservation.get_unassigned(),
            'waiting' : Reservation.get_waiting(),
            'on_going' : Reservation.get_on_going(),
            'all' : Reservation.objects.all().filter(time_reserved__gte=pd.Timestamp.now().floor('D'))
        }
        context['tables'] = _get_reservation_table()

        return context


    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseRedirect(reverse("home:home"))

        return super().get(request, *args, **kwargs)


    @staticmethod
    def post(request:WSGIRequest, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseRedirect(reverse("home:home"))

        try:
            match request.POST['type']:
                case 'assign':
                    ReservationView.assign(request.POST)
                case 'reservation':
                    ReservationView.reserve(request.POST)
                case 'confirm':
                    ReservationView.confirm(request.POST)
                case 'finished':
                    ReservationView.finish(request.POST)
                case 'delete':
                    ReservationView.delete(request.POST)
                case _:
                    pass
        except Exception as e:
            traceback.print_exception(e)

        return HttpResponseRedirect(reverse("reservation:home"))


    @staticmethod
    def reserve(payload:QueryDict):
        time_reserved = pd.Timestamp(f"{payload['date']} {payload['time_slot']}")

        phone = payload['phone'] or None if ("phone" in payload.keys()) else None
        # TODO: TABLE_ID
        reservation = Reservation(customer_name=payload['name'],
                                  customer_phone=phone,
                                  seat_count=payload['seat_count'],
                                  time_reserved=time_reserved)
        reservation.save()


    @staticmethod
    def assign(payload:QueryDict):
        reservation = Reservation.objects.get(id=payload['reservation'])

        for key, value in payload.items():
            if value == 'on':
                table = Table.objects.get(id=key)
                ReservationTable.objects.get_or_create(table=table, reservation=reservation)


    @staticmethod
    def confirm(payload:QueryDict):
        reservation = Reservation.objects.get(id=payload['reservation'])
        reservation.time_start = reservation.time_reserved

        reservation.save()


    @staticmethod
    def finish(payload:QueryDict):
        time_end = pd.Timestamp(f"{payload['time_slot']}")
        reservation = Reservation.objects.get(id=payload['reservation'])
        reservation.time_end = time_end

        reservation.save()


    @staticmethod
    def delete(payload:QueryDict):
        for key in payload.keys():
            if payload[key] == 'on':
                reservation = Reservation.objects.get(id=key)
                reservation.delete()


def _get_cell_content(r:Reservation)->_Cell:

    if r.time_start is None: # waiting, hasn't checked in
        return _Cell(r.customer_name, "darkorange")
    elif r.time_end is None: # ongoing, hasn't done
        return _Cell(r.customer_name, "red")
    elif r.time_end is not None:
        return _Cell(r.customer_name, "darkblue")

    return _Cell("Fehler", "red")


def get_time_opening():
    time_all = pd.date_range(start="12:00", end="20:30", freq='30min')

    weekday = pd.Timestamp.now().floor('D').weekday()

    SAT, SUN = 5, 6
    if weekday in [SAT, SUN]: # weekend no pause
        time_pause = pd.DatetimeIndex([])
    else:
        time_pause = pd.date_range(start="15:00", end="17:00", freq='30min', inclusive="left")

    return time_all.difference(time_pause)


def _get_reservation_table()->pd.DataFrame:
    time_opening = get_time_opening()

    table_ids = [table for table in Table.objects.all()]
    df = pd.DataFrame(index=table_ids, columns=time_opening)

    for reservation in Reservation.get_assigned():
        tables = reservation.tables
        # table_id = reservation.table

        if reservation.time_end is None:
            allocated_time = pd.date_range(start=reservation.time_reserved,
                                           end=reservation.time_reserved + pd.Timedelta(hours=2),
                                           freq='30min', inclusive='left') \
                                .tz_localize(None)
        else:
            allocated_time = pd.date_range(start=reservation.time_start,
                                           end=reservation.time_end,
                                           freq='30min')

        time = time_opening.intersection(allocated_time)

        cell_content = _get_cell_content(reservation)

        for table in tables:
            df.loc[table, time] = df.loc[table, time].fillna(value=cell_content)

    df = df.fillna(value=_Cell(False, "green"))

    df.columns = df.columns.strftime("%H:%M")

    return df


def _get_available_tables(reservation_id)->dict:
    """
    Get the list of available tables for this unassigned reservation
    :param reservation_id:
    :return:
    """

    reservation = Reservation.objects.get(id=reservation_id)
    free_tables = Table.get_free_at(reservation.time_reserved)

    to_json = lambda table: {
        "id": table.id,
        "seat_count": table.seat_count,
        "table_name": table.table_name,
    }

    big_tables = []
    small_tables = []

    for table in free_tables:
        if table.seat_count > 3:
            big_tables.append(to_json(table))
        else:
            small_tables.append(to_json(table))

    return {
        'big' : big_tables,
        'small' : small_tables,
    }


def ajax_reservation(request: WSGIRequest)->HttpResponse:
    """
    Register a new reservation from an ajax request from the external pages
    :param request: A WSGIRequest from an external page
    :return: 200 or 400 HTTP response
    """

    is_ajax = request.headers.get('X-Requested-With') == 'XMLHttpRequest'

    if is_ajax:
        if request.method == 'POST':
            try:
                ReservationView.reserve(request.POST)
                return HttpResponse(status=200)
            except Exception as e:
                traceback.print_exception(e)

                return HttpResponse(status=400)

    return HttpResponse(status=405)


def ajax_get_available_tables(request: WSGIRequest)->HttpResponse:
    """
    Given a reservation, return the available tables for it
    :param request: A WSGIRequest from the internal reservation page
    :return: A json object containing the available tables if legit request, otherwise redirect
    """

    is_ajax = request.headers.get('X-Requested-With') == 'XMLHttpRequest'
    if is_ajax:
        if request.user.is_authenticated:
            if request.method == 'GET':
                return JsonResponse(_get_available_tables(request.GET['reservation_id']))
            else:
                return HttpResponseRedirect("reservation:home", status=405)
        else:
            return HttpResponseRedirect("home:home", status=401)

    return HttpResponseRedirect("home:home", status=400)