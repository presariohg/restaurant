let lang_german = {
    "decimal" : ",",
    "emptyTable" : "Keine Daten",
    "thousands" : ".",
    "info" : "_START_ bis _END_ von _TOTAL_ Einträgen werden angezeigt.",
    "infoEmpty": "0 bis 0 von 0 Einträgen werden angezeigt",
    "search": "Suchen:",
    "lengthMenu" : "Zeigen _MENU_ Einträgen an",
    "zeroRecords" : "kein Ergebnis gefunden",
    "paginate" : {
        "first" : "1. Pos",
        "last" : "Ende",
        "next" : "Weiter",
        "previous" : "Zurück"
    }
}